module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  token: process.env.RENOVATE_TOKEN,

  logFileLevel: 'debug',

  repositories: [
    'uncycler/renovate-11710',
  ],

  requireConfig: true,
  onboarding: true,
  onboardingCommitMessage: "Configure Renovate",
  onboardingConfig: {
    enabledManagers: ["git-submodules"]
  },

  "git-submodules": {
    "enabled": true
  },
};
